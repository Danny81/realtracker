﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using UnityEngine.UI;

public class GetScreenPosition : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        ViveRoleSetter setter = other.gameObject.transform.parent.parent.gameObject.GetComponent<ViveRoleSetter>();
        Debug.Log("Canvas collided  with Tracker" + setter.viveRole.roleValue);
        /*Debug.Log("other collider (x): " + other.transform.position.x + " (y): " + other.transform.position.y +
                  " (z): " + other.transform.position.z);
        Debug.Log("my collider (x): " + transform.position.x + " (y): " + transform.position.y +
                  " (z): " + transform.position.z);*/
        Vector3 pointOnWallScreen = this.gameObject.GetComponent<MeshCollider>().ClosestPoint(other.transform.position);
        /*Debug.Log("closest point (x): " + pointOnWallScreen.x + " (y): " + pointOnWallScreen.y +
                  " (z): " + pointOnWallScreen.z);*/

        GameObject.Find("Tracker" + setter.viveRole.roleValue).GetComponent<TrackerHandler>()
            .lastCollisionPointWithWallScreen = pointOnWallScreen;


    }

    void OnTriggerStay(Collider other)
    {
        ViveRoleSetter setter = other.gameObject.transform.parent.parent.gameObject.GetComponent<ViveRoleSetter>();
        Debug.Log("[OnTriggerStay ]Canvas collided  with Tracker" + setter.viveRole.roleValue);
        /*Debug.Log("other collider (x): " + other.transform.position.x + " (y): " + other.transform.position.y +
                  " (z): " + other.transform.position.z);
        Debug.Log("my collider (x): " + transform.position.x + " (y): " + transform.position.y +
                  " (z): " + transform.position.z);*/
        Vector3 pointOnWallScreen = this.gameObject.GetComponent<MeshCollider>().ClosestPoint(other.transform.position);
        /*Debug.Log("closest point (x): " + pointOnWallScreen.x + " (y): " + pointOnWallScreen.y +
                  " (z): " + pointOnWallScreen.z);*/

        GameObject.Find("Tracker" + setter.viveRole.roleValue).GetComponent<TrackerHandler>()
            .lastCollisionPointWithWallScreen = pointOnWallScreen;


    }

}
