﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.source
{
    public static class TransferTabletContentUtilities
    {
        public static float GetPointRelativeOnWallScreenX(GameObject wallScreen, Vector3 pointOnWallScreen)
        {
            //Planes in unity always 10x10 meters by default
            var widthWallDisplay = 10 * wallScreen.transform.localScale.x;
            float x = wallScreen.transform.position.z + widthWallDisplay / 2;
            float distanceX = Mathf.Abs(x - pointOnWallScreen.z) / widthWallDisplay;

            Debug.Log("distance X is " + distanceX);
            return distanceX;
        }

        public static float GetPointRelativeOnWallScreenY(GameObject wallScreen, Vector3 pointOnWallScreen)
        {
            //Planes in unity always 10x10 meters by default
            var heightWallDisplay = 10 * wallScreen.transform.localScale.z;
            float y = wallScreen.transform.position.y + heightWallDisplay / 2;
            float distanceY = Mathf.Abs(y - pointOnWallScreen.y) / heightWallDisplay;

            Debug.Log("distance Y is " + distanceY);
            return distanceY;
        }
    }
}
