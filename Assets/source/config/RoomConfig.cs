﻿namespace RealTracker
{
    public static class RoomConfig
    {
        public struct PersonalInteractionZoneTreshholds
        {
            /*public static float xLowerLimit = 0.8f;
            public static float xUpperLimit = 1.6f;
            public static float yLowerLimit = 0.6f;
            public static float yUpperLimit = 1.5f;
            public static float zLowerLimit = -0.7f;
            public static float zUpperLimit = 1f;*/

            public static float xLowerLimit = -1.5f;
            public static float xUpperLimit = 1.0f;
            public static float yLowerLimit = 0.6f;
            public static float yUpperLimit = 1.7f;
            public static float zLowerLimit = 1.5f;
            public static float zUpperLimit = 3.0f;
        }

        public struct SubtleInteractionZoneTreshholds
        {
            /*public static float xLowerLimit = 0.8f;
            public static float xUpperLimit = 1.6f;
            public static float yLowerLimit = 0.6f;
            public static float yUpperLimit = 1.5f;
            public static float zLowerLimit = -0.7f;
            public static float zUpperLimit = 1f;*/

            public static float xLowerLimit = -1.5f;
            public static float xUpperLimit = 1.5f;
            public static float yLowerLimit = 0.6f;
            public static float yUpperLimit = 1.7f;
            public static float zLowerLimit = 0.0f;
            public static float zUpperLimit =1.5f;
        }

    }
}