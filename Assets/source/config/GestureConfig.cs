﻿namespace RealTracker
{
    public static class GestureConfig
    {
        public struct HoldingInFrontOfScreenTrackerBackside
        {
            public static float posYlowerLimit = 0.6f;
            public static float posYupperLimit = 1.5f;
            /*public static float rotXlowerLimit = 0f;
            public static float rotXupperLimit = 70f;
            public static float rotYlowerLimit = 20f;
            public static float rotYupperLimit = 120f;
            public static float rotZlowerLimit1 = 0f;
            public static float rotZupperLimit1 = 20f;
            public static float rotZlowerLimit2 = 340f;
            public static float rotZupperLimit2 = 360f;*/

            public static float rotXlowerLimit = 0f;
            public static float rotXupperLimit = 70f;
            public static float rotYlowerLimit1 = 0f;
            public static float rotYupperLimit1 = 40f;
            public static float rotYlowerLimit2 = 300f;
            public static float rotYupperLimit2 = 360f;
            public static float rotZlowerLimit1 = 0f;
            public static float rotZupperLimit1 = 20f;
            public static float rotZlowerLimit2 = 340f;
            public static float rotZupperLimit2 = 360f;

        }

        public struct HoldingInFrontOfScreenTrackerTop
        {
            public static float posYlowerLimit = 0.6f;
            public static float posYupperLimit = 1.5f;
            /*public static float rotXlowerLimit = 0f;
            public static float rotXupperLimit = 70f;
            public static float rotYlowerLimit = 20f;
            public static float rotYupperLimit = 120f;
            public static float rotZlowerLimit1 = 0f;
            public static float rotZupperLimit1 = 20f;
            public static float rotZlowerLimit2 = 340f;
            public static float rotZupperLimit2 = 360f;*/

            public static float rotXlowerLimit = 270f;
            public static float rotXupperLimit = 360f;
            public static float rotYlowerLimit1 = 0f;
            public static float rotYupperLimit1 = 60f;
            public static float rotYlowerLimit2 = 320f;
            public static float rotYupperLimit2 = 360f;
            public static float rotZlowerLimit1 = 160f;
            public static float rotZupperLimit1 = 200f;
           // public static float rotZlowerLimit2 = 340f;
            //public static float rotZupperLimit2 = 360f;

        }


        public struct HoldingToScreenTrackerBackside
        {
            /*public static float posYlowerLimit = 0.6f;
            public static float posYupperLimit = 1.6f;
            public static float rotXlowerLimit = 20f;
            public static float rotXupperLimit = 30f;
            public static float rotYlowerLimit = 220f;
            public static float rotYupperLimit = 280f;
            public static float rotZlowerLimit1 = 170;
            public static float rotZupperLimit1 = 190;*/
            // public static float rotZlowerLimit2 = 0f;
            // public static float rotZupperLimit2 = 10f;
            public static float posYlowerLimit = 0.6f;
            public static float posYupperLimit = 1.6f;
            public static float rotXlowerLimit = 60f;
            public static float rotXupperLimit = 90f;
            public static float rotYlowerLimit = 120;
            public static float rotYupperLimit = 220;
            public static float rotZlowerLimit1 = 170;
            public static float rotZupperLimit1 = 190;
        }

        public struct HoldingToScreenTrackerTop
        {
            /*public static float posYlowerLimit = 0.6f;
            public static float posYupperLimit = 1.6f;
            public static float rotXlowerLimit = 20f;
            public static float rotXupperLimit = 30f;
            public static float rotYlowerLimit = 220f;
            public static float rotYupperLimit = 280f;
            public static float rotZlowerLimit1 = 170;
            public static float rotZupperLimit1 = 190;*/
            // public static float rotZlowerLimit2 = 0f;
            // public static float rotZupperLimit2 = 10f;
            public static float posYlowerLimit = 0.6f;
            public static float posYupperLimit = 1.6f;
            public static float rotXlowerLimit = 70f;
            public static float rotXupperLimit = 90f;
            public static float rotYlowerLimit1 = 0;
            public static float rotYupperLimit1 = 60;
            public static float rotYlowerLimit2 = 320;
            public static float rotYupperLimit2 = 360;
            public static float rotZlowerLimit1 = 160;
            public static float rotZupperLimit1 = 200;
        }

    }
}