﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Source;
using HiveTrackingServerSocket;
using UnityEngine;

namespace Assets.source
{
    public static class NodeWallDisplayPressedUtilities
    {

        /*transfers the the browser pixel coordinates of the pressed node on the wall screen
            into Unity world Coordinates matching the PHYSICAL point on the Wall Display*/
        public static Vector3 getWorldCoordinatesfromBrowserCoordinates(GameObject wallDisplay, MessageAttributesNodeWallDisplayPressed nodePressedArgs)
        {
            Vector3 displayGlobalPosition = wallDisplay.transform.position;

            //Planes in unity always 10x10 meters by default
            var widthWallDisplay = 10 * wallDisplay.transform.localScale.x;
            var heightWallDisplay = 10 * wallDisplay.transform.localScale.z; //z is height because plane is flipped in browser

            Debug.Log("[getWorldCoordinatesfromBrowserCoordinates] Global coordinates wall display x: " + displayGlobalPosition.x + " y: " + displayGlobalPosition.y + " z: " + displayGlobalPosition.z);

            var relativePosXinBrowser = nodePressedArgs.xAbsolute / nodePressedArgs.screenResolutionX;
            var relativePosYinBrowser = nodePressedArgs.yAbsolute / nodePressedArgs.screenResolutionY;

            Debug.Log(" [getWorldCoordinatesfromBrowserCoordinates] relativePosXinBrowser: " + relativePosXinBrowser + " width Wall Display (m): " + widthWallDisplay);
            Debug.Log(" [getWorldCoordinatesfromBrowserCoordinates] relativePosYinBrowser: " + relativePosYinBrowser + " height Wall Display (m): " + heightWallDisplay);


            //offset to be subtracted from left/upper edge of wall screen to match corrrect sceen position
            var physicalXposRelativeOnWallDisplay = widthWallDisplay * relativePosXinBrowser;
            var physicalYposRelativeOnWallDisplay = heightWallDisplay * relativePosYinBrowser;

            Debug.Log("[getWorldCoordinatesfromBrowserCoordinates] x-offset on wall display: " + physicalXposRelativeOnWallDisplay);
            Debug.Log("[getWorldCoordinatesfromBrowserCoordinates] y-offset on wall display: " + physicalYposRelativeOnWallDisplay);

            var xPosOnWallDisplay = displayGlobalPosition.z + widthWallDisplay / 2 - physicalXposRelativeOnWallDisplay;
            var yPosOnWallDisplay = displayGlobalPosition.y + heightWallDisplay / 2 - physicalYposRelativeOnWallDisplay;

            Debug.Log("[getWorldCoordinatesfromBrowserCoordinates] xPosOnWallDisplay: " + xPosOnWallDisplay + " yPosOnWallDisplay: " + yPosOnWallDisplay);

            /*dirty hack for now: x pixel coordinate of screen is z coordinate in unity editor
             - x coordinate of Vector remains unchanges as it represents a z coordinate whoch is not relevant for a 2D node
             press on screen */
            return new Vector3(displayGlobalPosition.x, yPosOnWallDisplay, xPosOnWallDisplay);
        }

        /*search the nearest ative tracker to the wall screen position where the node was pressed to open the details-on-demand view
         on the tracker's tablet*/
        public static GameObject getClosestTrackerToWallScreenPosition(Vector3 wallScreenPos,
            Dictionary<GameObject, bool> trackers)
        {
            GameObject nearestTracker = null;
            List<KeyValuePair<GameObject, float>> trackersByDistance = new List<KeyValuePair<GameObject, float>>();

            foreach (var tracker in trackers)
            {
                if (!tracker.Value)
                {
                    Debug.Log("[getClosestTrackerToWallScreenPosition] Tracker " + tracker.Key.name + " not connected...");
                }

                else
                {
                    //consider only trackers belonging to users who realisitcally pressed the nod ein front of the wall display
                    if (TrackerGestures.TrackerInPersonalInteractionZone(tracker.Key.transform.position))
                    {
                        var distance = Vector3.Distance(wallScreenPos, tracker.Key.transform.position);
                        trackersByDistance.Add(new KeyValuePair<GameObject, float>(tracker.Key, distance));
                    }
                    else
                    {
                        Debug.Log("[getClosestTrackerToWallScreenPosition] Tracker " + tracker.Key.name + " not considered, is not close to wall screen...");
                    }

                }
            }

            if (trackersByDistance.Count > 0)
            {
                trackersByDistance.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var closestTracker = trackersByDistance.First();

                Debug.Log("[getClosestTrackerToWallScreenPosition] closest tracker " + closestTracker.Key.name + ", distance " + closestTracker.Value);
                nearestTracker = closestTracker.Key;
            }
            else
            {
                Debug.Log("[getClosestTrackerToWallScreenPosition] no valid tracker found...");
            }

            return nearestTracker;
        }

    }
}
