﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.source
{
    public class TrackerIsValidChangedEventArgs : EventArgs
    {
        public string trackerName { get; set; }
        public bool newState { get; set; }

        public TrackerIsValidChangedEventArgs(string name, bool state)
        {
            trackerName = name;
            newState = state;
        }

    }
}
