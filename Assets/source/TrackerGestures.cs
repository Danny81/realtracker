﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.source.config;
using RealTracker;
using UnityEngine;

namespace Assets.Source
{
    public static class TrackerGestures
    {

        public static bool TrackerOnTabletFacingScreen(Vector3 trackerPosition, Vector3 trackerRotation, PhysicalConfig.TabletTrackerMountPositions trackerPostion)
        {

            switch (trackerPostion)
            {
                case PhysicalConfig.TabletTrackerMountPositions.Backside:

                    return trackerRotation.x >= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotXlowerLimit &&
                           trackerRotation.x <= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotXupperLimit &&
                           ((trackerRotation.y >= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotYlowerLimit1 &&
                             trackerRotation.y <= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotYupperLimit1) ||
                            (trackerRotation.y >= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotYlowerLimit2 &&
                             trackerRotation.y <= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotYupperLimit2)
                           ) &&
                           (
                               (trackerRotation.z >= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotZlowerLimit1 &&
                                trackerRotation.z <= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotZupperLimit1)
                               ||
                               (trackerRotation.z >= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotZlowerLimit2 &&
                                trackerRotation.z <= GestureConfig.HoldingInFrontOfScreenTrackerBackside.rotZupperLimit2
                               )
                           ) &&
                           trackerPosition.y >= GestureConfig.HoldingInFrontOfScreenTrackerBackside.posYlowerLimit &&
                           trackerPosition.y <= GestureConfig.HoldingInFrontOfScreenTrackerBackside.posYupperLimit;

                    break;
                case PhysicalConfig.TabletTrackerMountPositions.Top:
                    return trackerRotation.x >= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotXlowerLimit &&
                           trackerRotation.x <= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotXupperLimit &&
                           ((trackerRotation.y >= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotYlowerLimit1 &&
                             trackerRotation.y <= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotYupperLimit1) ||
                            (trackerRotation.y >= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotYlowerLimit2 &&
                             trackerRotation.y <= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotYupperLimit2)
                           ) &&
                           /*(
                               (*/trackerRotation.z >= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotZlowerLimit1 &&
                                trackerRotation.z <= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotZupperLimit1/*)
                               ||
                               (trackerRotation.z >= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotZlowerLimit2 &&
                                trackerRotation.z <= GestureConfig.HoldingInFrontOfScreenTrackerTop.rotZupperLimit2
                               )
                           )*/ &&
                           trackerPosition.y >= GestureConfig.HoldingInFrontOfScreenTrackerTop.posYlowerLimit &&
                           trackerPosition.y <= GestureConfig.HoldingInFrontOfScreenTrackerTop.posYupperLimit;
                    break;
                default:
                    throw new NotImplementedException("Not implemented Tracker Position on Tablet");
                    break;
            }

        }

        public static bool TrackerInSubtleInteractionZone(Vector3 trackerPosition)
        {

            return trackerPosition.x >= RoomConfig.SubtleInteractionZoneTreshholds.xLowerLimit &&
                   trackerPosition.x <= RoomConfig.SubtleInteractionZoneTreshholds.xUpperLimit &&
                   trackerPosition.y >= RoomConfig.SubtleInteractionZoneTreshholds.yLowerLimit &&
                   trackerPosition.y <= RoomConfig.SubtleInteractionZoneTreshholds.yUpperLimit &&
                   trackerPosition.z >= RoomConfig.SubtleInteractionZoneTreshholds.zLowerLimit &&
                   trackerPosition.z <= RoomConfig.SubtleInteractionZoneTreshholds.zUpperLimit;
        }

        public static bool TrackerInPersonalInteractionZone(Vector3 trackerPosition)
        {

            return trackerPosition.x >= RoomConfig.PersonalInteractionZoneTreshholds.xLowerLimit &&
                   trackerPosition.x <= RoomConfig.PersonalInteractionZoneTreshholds.xUpperLimit &&
                   trackerPosition.y >= RoomConfig.PersonalInteractionZoneTreshholds.yLowerLimit &&
                   trackerPosition.y <= RoomConfig.PersonalInteractionZoneTreshholds.yUpperLimit &&
                   trackerPosition.z >= RoomConfig.PersonalInteractionZoneTreshholds.zLowerLimit &&
                   trackerPosition.z <= RoomConfig.PersonalInteractionZoneTreshholds.zUpperLimit;
        }

        public static bool TrackerOnTabletHoldTowardsScreen(Vector3 trackerPosition, Vector3 trackerRotation, PhysicalConfig.TabletTrackerMountPositions trackerPostion)
        {

            switch (trackerPostion)
            {
                case PhysicalConfig.TabletTrackerMountPositions.Backside:
                    return trackerRotation.x >= GestureConfig.HoldingToScreenTrackerBackside.rotXlowerLimit &&
                           trackerRotation.x <= GestureConfig.HoldingToScreenTrackerBackside.rotXupperLimit &&
                           trackerRotation.y >= GestureConfig.HoldingToScreenTrackerBackside.rotYlowerLimit &&
                           trackerRotation.y <= GestureConfig.HoldingToScreenTrackerBackside.rotYupperLimit &&
                           (
                               (trackerRotation.z >= GestureConfig.HoldingToScreenTrackerBackside.rotZlowerLimit1 &&
                                trackerRotation.z <= GestureConfig.HoldingToScreenTrackerBackside.rotZupperLimit1)
                               /*||
                               (trackerRotation.z >= GestureConfig.HoldingToScreen.rotZlowerLimit2 &&
                                trackerRotation.z <= GestureConfig.HoldingToScreen.rotZupperLimit2)*/
                           ) && trackerPosition.y >= GestureConfig.HoldingToScreenTrackerBackside.posYlowerLimit &&
                           trackerPosition.y <= GestureConfig.HoldingToScreenTrackerBackside.posYupperLimit;
                    break;
                case PhysicalConfig.TabletTrackerMountPositions.Top:
                    return trackerRotation.x >= GestureConfig.HoldingToScreenTrackerTop.rotXlowerLimit &&
                           trackerRotation.x <= GestureConfig.HoldingToScreenTrackerTop.rotXupperLimit &&
                           (
                               (trackerRotation.y >= GestureConfig.HoldingToScreenTrackerTop.rotYlowerLimit1 &&
                                trackerRotation.y <= GestureConfig.HoldingToScreenTrackerTop.rotYupperLimit1)
                               ||
                               (trackerRotation.y >= GestureConfig.HoldingToScreenTrackerTop.rotYlowerLimit2 &&
                                trackerRotation.y <= GestureConfig.HoldingToScreenTrackerTop.rotYupperLimit2)
                           ) &&
                           (
                               (trackerRotation.z >= GestureConfig.HoldingToScreenTrackerTop.rotZlowerLimit1 &&
                                trackerRotation.z <= GestureConfig.HoldingToScreenTrackerTop.rotZupperLimit1)
                               /*||
                               (trackerRotation.z >= GestureConfig.HoldingToScreen.rotZlowerLimit2 &&
                                trackerRotation.z <= GestureConfig.HoldingToScreen.rotZupperLimit2)*/
                           ) && trackerPosition.y >= GestureConfig.HoldingToScreenTrackerTop.posYlowerLimit &&
                           trackerPosition.y <= GestureConfig.HoldingToScreenTrackerTop.posYupperLimit;
                    break;
                default:
                    throw new NotImplementedException("Not implemented Tracker Position on Tablet");
                    break;
            }

        }


    }
}
