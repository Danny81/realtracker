﻿using System;
using HiveTrackingServerSocket;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Assets.source;
using RealTracker;
using UnityEngine;

public class InteractionsManager : MonoBehaviour
{

    public ServerSocketHandler socket;
    public Dictionary<GameObject, bool> trackers;
    public GameObject wallDisplay;
    public bool NodeWallDisplayPressedReceived = false;
    public MessageAttributesNodeWallDisplayPressed nodePressedArgs;
    public GameObject nodePressTestCube;

    // Start is called before the first frame update
    void Start()
    {
        var socketObject = GameObject.Find("SocketServer");
        socket = socketObject.GetComponent<ServerSocketHandler>();
        socket.serverSocket.NodeOnWallDisplayPressed += NodeWallDisplayPressed;

        trackers = new Dictionary<GameObject, bool>();

        var configuredTrackers = GameObject.FindGameObjectsWithTag("activeTracker");

        foreach (var configuredTracker in configuredTrackers)
        {
            var trackerScript = configuredTracker.GetComponent<TrackerHandler>();
            trackerScript.TrackerIsValidChanged += TrackerStateChanged;
            trackers.Add(configuredTracker, false);
        }

        Debug.Log("InteractionsManager: added " + trackers.Count + " configured trackers");

    }

    // Update is called once per frame
    void Update()
    {
        //do this only once after event was received
        if (NodeWallDisplayPressedReceived)
        {

            /*calculate world coordinate of node press on wall screen with pixel coordinates of click/press
            & wall display size / position / orientation*/
            try
            {

                /*test cube in unity editor that should move to the according position on the 'wall screen' plane
                 based on node press in browser*/
                var realWorldPositionNodePress = NodeWallDisplayPressedUtilities.getWorldCoordinatesfromBrowserCoordinates(wallDisplay,
                    nodePressedArgs);
                nodePressTestCube.transform.position = realWorldPositionNodePress;

                var closestTracker =
                    NodeWallDisplayPressedUtilities.getClosestTrackerToWallScreenPosition(realWorldPositionNodePress,
                        trackers);

                //send to tablet browser instance(if registered) when a tracker in reach was found
                if (closestTracker != null)
                {

                    var sessionID =socket.serverSocket.devices.TryGetSessionIdFromDevice(closestTracker.name);
                    if (sessionID != null)
                    {
                        var msg = socket.serverSocket.devices.BuildMsgShowNodeDetailsToDevice("WallDisplay",
                            closestTracker.name, nodePressedArgs.nodeID, nodePressedArgs.detectionTime);
                        socket.serverSocket.webSocket.WebSocketServices["/trackinggateway"].Sessions.SendTo(msg, sessionID);
                    }
                    else
                    {
                        Debug.LogWarning("Tracker " + closestTracker.name + " has no registered session in WebSocket");
                    }

                    //socket.serverSocket.devices.SendShowNodeDetailsToDevice("WallDisplay", closestTracker.name, nodePressedArgs.nodeID);
                }

            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }

            NodeWallDisplayPressedReceived = false;
        }
    }

    public void NodeWallDisplayPressed(object sender, NodeWallDisplayPressedEventArgs e)
    {
        Debug.Log("NodeWallDisplayPressed event received from socket: " + e.pressedNodeMessage.ToJSON());

        /* Unity methods essential for distance calculation unfortunately
         only available in main thread - just notify the update method that there is sth. to do*/
        NodeWallDisplayPressedReceived = true;
        nodePressedArgs = e.pressedNodeMessage.NodeWallDisplayPressedAttributes;

    }

    public void TrackerStateChanged(object sender, TrackerIsValidChangedEventArgs e)
    {
        Debug.Log("InteractionsManager: received new IsValidState from " + e.trackerName + ": " + e.newState);

        var tracker = GameObject.Find(e.trackerName);

        if (tracker != null)
        {

            if (trackers.ContainsKey(tracker))
            {
                trackers[tracker] = e.newState;
                Debug.Log("InteractionsManager: state of tracker " + e.trackerName + " changed from " + !e.newState +
                          " to " + e.newState);
            }
            else
            {
                Debug.LogWarning("InteractionsManager: event from unknown tracker received");
            }
        }
        else
        {
            Debug.LogWarning("InteractionsManager: IsValidChangedEvent from unknown tracker received");
        }
    }

}
