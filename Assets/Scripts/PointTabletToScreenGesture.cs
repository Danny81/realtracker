﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.source;
using Assets.Source;
using Assets.source.config;
using Assets.source.utils;
using UnityEngine;
using UnityEngine.Serialization;
using Valve.VR;

namespace Assets.Scripts
{
    public class PointTabletToScreenGesture
    {

        private bool inFrontScreen;
        private bool holdingFacingScreen;
        private bool holdingTowardsScreen;

        private float coolDownPhaseTimeout;
        private bool coolingDown;

        private const float holdingFacingWallDisplayTimeout = 3f;
        private const float holdingTowardsWallDisplayTimeout = 2f;

        private float holdingFacingScreenTimeOut = HoldingFacingWallDisplayTimeout;
        private float holdingTowardsScreenTimeOut = HoldingTowardsWallDisplayTimeout;

        private GameObject thisObject;

        private  InteractionsManager interactionsManager;

        public PointTabletToScreenGesture(GameObject go)
        {
            InFrontScreen = false;
            HoldingFacingScreen = false;
            HoldingTowardsScreen = false;
            CoolDownPhaseTimeout = 1f;
            CoolingDown = false;
            thisObject = go;

            var interactionstObject = GameObject.Find("InteractionsManager");
            interactionsManager = interactionstObject.GetComponent<InteractionsManager>();

        }

        public bool InFrontScreen
        {
            get => inFrontScreen;
            private set => inFrontScreen = value;
        }

        public bool HoldingFacingScreen
        {
            get => holdingFacingScreen;
            private set => holdingFacingScreen = value;
        }

        public bool HoldingTowardsScreen
        {
            get => holdingTowardsScreen;
            private set => holdingTowardsScreen = value;
        }

        public float CoolDownPhaseTimeout
        {
            get => coolDownPhaseTimeout;
            private set => coolDownPhaseTimeout = value;
        }

        public bool CoolingDown
        {
            get => coolingDown;
            private set => coolingDown = value;
        }

        public static float HoldingFacingWallDisplayTimeout => holdingFacingWallDisplayTimeout;

        public static float HoldingTowardsWallDisplayTimeout => holdingTowardsWallDisplayTimeout;

        public float HoldingFacingScreenTimeOut
        {
            get => holdingFacingScreenTimeOut;
            private set => holdingFacingScreenTimeOut = value;
        }

        public float HoldingTowardsScreenTimeOut
        {
            get => holdingTowardsScreenTimeOut;
            private set => holdingTowardsScreenTimeOut = value;
        }

        public bool checkForTabletPointedTowardsScreenGesture(Vector3 currPosition, Vector3 currRotation, DateTime possibleGestureDetectionTime, PhysicalConfig.TabletTrackerMountPositions trackerPostion)
        {

            bool gestureDetected = false;

            if (doCheckStillCoolingDown())
            {
                return gestureDetected;
            }

            doCheckHoldingFacingScreen(currPosition, currRotation, trackerPostion);
            doCheckTrackerNearToWallDisplay(currPosition, trackerPostion);
            doCheckTrackerHoldTowardsWallDisplay(currPosition, currRotation, trackerPostion);

            if (HoldingTowardsScreen && HoldingFacingScreen && InFrontScreen)
            {
                gestureDetected = true;
                triggerGesture(possibleGestureDetectionTime);
            }

            return gestureDetected;

        }

        private bool doCheckStillCoolingDown()
        {
            bool stillCoolingDown;
            if (CoolingDown)
            {
                CoolDownPhaseTimeout -= Time.deltaTime;
                if (CoolDownPhaseTimeout < 0)
                {
                    Debug.Log("Cooldown ended");
                    CoolDownPhaseTimeout = 1.0f;
                    CoolingDown = false;
                    stillCoolingDown = CoolingDown;
                }
                else
                {
                    stillCoolingDown = true;
                }

            }
            else
            {
                stillCoolingDown = false;
            }

            return stillCoolingDown;
        }

        private void doCheckHoldingFacingScreen(Vector3 currPosition, Vector3 currRotation, PhysicalConfig.TabletTrackerMountPositions trackerPostion)
        {
            bool gesturePresent = TrackerGestures.TrackerOnTabletFacingScreen(currPosition, currRotation, trackerPostion);
            //Debug.Log("Tablet facing to screen? " + gesturePresent);
            if (gesturePresent)
            {

                HoldingFacingScreenTimeOut = HoldingFacingWallDisplayTimeout;
                if (!HoldingFacingScreen)
                {
                    Debug.Log("User holding tablet with tracker " + thisObject.name +
                              " and facing the wall screen...");
                    HoldingFacingScreen = true;
                }
            }
            else
            {
                if (HoldingFacingScreen)
                {
                    HoldingFacingScreenTimeOut -= Time.deltaTime;
                    if (HoldingFacingScreenTimeOut < 0)
                    {
                        Debug.Log("User holding tablet with tracker " + thisObject.name + " NOT facing wall screen anymore...");
                        HoldingFacingScreen = false;
                        HoldingFacingScreenTimeOut = HoldingFacingWallDisplayTimeout;
                    }
                }
            }

        }

        private void doCheckTrackerNearToWallDisplay(Vector3 currPosition, PhysicalConfig.TabletTrackerMountPositions trackerPostion)
        {
            bool positionPresent = TrackerGestures.TrackerInSubtleInteractionZone(currPosition) || TrackerGestures.TrackerInPersonalInteractionZone(currPosition);

            if (positionPresent)
            {
                if (!InFrontScreen)
                {
                    Debug.Log("Tracker " + thisObject.name + " entered wall screen front area");
                    InFrontScreen = true;
                }
            }
            else
            {
                if (InFrontScreen)
                {
                    Debug.Log("Tracker " + thisObject.name + " left wall screen front area");
                    InFrontScreen = false;
                }
            }

        }

        private void doCheckTrackerHoldTowardsWallDisplay(Vector3 currPosition, Vector3 currRotation, PhysicalConfig.TabletTrackerMountPositions trackerPostion)
        {
            bool gesturePresent = TrackerGestures.TrackerOnTabletHoldTowardsScreen(currPosition, currRotation, trackerPostion);

            if (gesturePresent)
            {
                HoldingTowardsScreenTimeOut = HoldingTowardsWallDisplayTimeout;
                if (!HoldingTowardsScreen)
                {
                    Debug.Log("User holding tablet with tracker " + thisObject.name + " towards the wall screen...");
                    HoldingTowardsScreen = true;
                }
            }
            else
            {
                if (HoldingTowardsScreen)
                {
                    HoldingTowardsScreenTimeOut -= Time.deltaTime;
                    if (HoldingTowardsScreenTimeOut < 0)
                    {
                        Debug.Log("User holding tablet with tracker " + thisObject.name + " NOT towards screen anymore...");
                        HoldingTowardsScreen = false;
                        HoldingTowardsScreenTimeOut = HoldingTowardsWallDisplayTimeout;
                    }
                }
            }

        }

        private void triggerGesture(DateTime detectedTime)
        {
            Debug.Log("TABLET SHARING CONTENT TO WALL SCREEN - GameObject " + thisObject.name);
            var trackerScript = thisObject.GetComponent<TrackerHandler>();

            var screenPosX =
                TransferTabletContentUtilities.GetPointRelativeOnWallScreenX(GameObject.Find("WallDisplay"),
                    trackerScript.lastCollisionPointWithWallScreen);
            var screenPosY =
                TransferTabletContentUtilities.GetPointRelativeOnWallScreenY(GameObject.Find("WallDisplay"),
                    trackerScript.lastCollisionPointWithWallScreen);

            var sessionID = interactionsManager.socket.serverSocket.devices.TryGetSessionIdFromDevice(thisObject.name);
            if (sessionID != null)
            {
                var javaScriptTime = DateTimeHelper.ToJavaScriptMilliseconds(detectedTime);
                var msg = interactionsManager.socket.serverSocket.devices.BuildMsgRequestTabletContentFromDevice(thisObject.name, screenPosX, screenPosY, javaScriptTime);
                interactionsManager.socket.serverSocket.webSocket.WebSocketServices["/trackinggateway"].Sessions.SendTo(msg, sessionID);
            }
            else
            {
                Debug.LogWarning("Tracker " + thisObject.name + " has no registered session in WebSocket");
            }

            HoldingTowardsScreen = false;
            HoldingFacingScreen = false;
            HoldingFacingScreenTimeOut = HoldingFacingWallDisplayTimeout;
            HoldingTowardsScreenTimeOut = HoldingTowardsWallDisplayTimeout;
            CoolingDown = true;
        }


    }
}
