﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Policy;
using System.Text;
using RealTracker;
using UnityEngine;
using Valve.VR;

public class writeTrackingDataToFile : MonoBehaviour
{
    public string filePath;

    public GameObject thisObject;
    public StringBuilder sb;

    public const float secondsToSample = 3f;

    public float sampleTimer;

    public bool doRecord = false;

    public SteamVR_Action_Boolean grabPinch; //Grab Pinch is the trigger, select from inspecter
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;//which controller

    // Start is called before the first frame update
    void Start()
    {
        thisObject = this.gameObject;
        filePath = TrackingDataLogger.retrieveFilePath(thisObject);
        sb = new StringBuilder();
        sampleTimer = secondsToSample;
    }

    // Update is called once per frame
    void Update()
    {

        if (sampleTimer >= 0 && doRecord)
        {

            float[] output = TrackingDataLogger.retrieveCommonTrackingData(thisObject);
            sb = new StringBuilder();
            sb = TrackingDataLogger.AddDataToLine(sb, output);

            if (!File.Exists(filePath))
            {
                File.WriteAllText(filePath, sb.ToString());
            }
            else
            {
                File.AppendAllText(filePath, sb.ToString());
            }

            sampleTimer -= Time.deltaTime;

            if (sampleTimer <= 0)
            {
                doRecord = false;
            }

        }

    }

    void OnEnable()
    {
        if (grabPinch != null)
        {
            grabPinch.AddOnChangeListener(OnTriggerPressedOrReleased, inputSource);
        }
    }


    private void OnDisable()
    {
        if (grabPinch != null)
        {
            grabPinch.RemoveOnChangeListener(OnTriggerPressedOrReleased, inputSource);
        }
    }

    private void OnTriggerPressedOrReleased(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
    {

        Debug.Log("Trigger was pressed or released");
        if (!doRecord)
        {
            Debug.Log("New measurement wil start...");
            doRecord = true;
            sampleTimer = secondsToSample;
            filePath = TrackingDataLogger.retrieveFilePath(thisObject);
        }

    }

}
