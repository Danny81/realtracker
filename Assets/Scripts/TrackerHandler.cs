﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.source;
using Assets.source.config;
using HiveTrackingServerSocket;
using RealTracker;
using UnityEngine;
using UnityEngine.Serialization;

public class TrackerHandler : MonoBehaviour
{

    public const PhysicalConfig.TabletTrackerMountPositions trackerMountPostion =
        PhysicalConfig.TabletTrackerMountPositions.Top;

    public GameObject thisObject;
    public Vector3 trackerRotation;
    public Vector3 trackerPosition;

    public bool inFrontScreen;
    public bool holdingFacingScreen;
    public bool holdingTowardsScreen;

    public float coolDownPhaseTimeout;

    public float holdingFacingWallDisplayTimeout;
    public float holdingTowardsWallDisplayTimeout;

    public GameObject serverSocket;
    private ServerSocketHandler socketHandler;

    public AudioClip contentShared;
    AudioSource audioSource;

    private PointTabletToScreenGesture checkContentShareGesture;

    public delegate void TrackerIsValidChange(object sender, TrackerIsValidChangedEventArgs e);
    public event TrackerIsValidChange TrackerIsValidChanged;

    public Vector3 lastCollisionPointWithWallScreen;

    //for latency measurement
    public DateTime possibleGestureDetectionTime;

    // Start is called before the first frame update
    void Start()
    {
        thisObject = this.gameObject;
        audioSource = GetComponent<AudioSource>();
        checkContentShareGesture = new PointTabletToScreenGesture(thisObject);
        socketHandler = serverSocket.GetComponent<ServerSocketHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        possibleGestureDetectionTime = DateTime.Now;
        trackerPosition = thisObject.transform.position;
        trackerRotation = thisObject.transform.rotation.eulerAngles;

        if (checkContentShareGesture.checkForTabletPointedTowardsScreenGesture(trackerPosition, trackerRotation, possibleGestureDetectionTime, trackerMountPostion))
        {
            audioSource.PlayOneShot(contentShared, 1.0F);
        }

        updateGameObjectVarsForMonitoring();

    }

    public virtual void OnTrackerIsValidChanged(object sender, TrackerIsValidChangedEventArgs e)
    {
        TrackerIsValidChanged?.Invoke(this, e);
    }

    public void updateGameObjectVarsForMonitoring()
    {
        inFrontScreen = checkContentShareGesture.InFrontScreen;
        holdingFacingScreen = checkContentShareGesture.HoldingFacingScreen;
        holdingTowardsScreen = checkContentShareGesture.HoldingTowardsScreen;
        coolDownPhaseTimeout = checkContentShareGesture.CoolDownPhaseTimeout;
        holdingFacingWallDisplayTimeout = checkContentShareGesture.HoldingFacingScreenTimeOut;
        holdingTowardsWallDisplayTimeout = checkContentShareGesture.HoldingTowardsScreenTimeOut;
    }

    public void OnIsValidChanged(bool newValue)
    {
        Debug.Log("OnIsValidChanged: device " + this.gameObject.name + " new state: " + newValue);
        TrackerIsValidChangedEventArgs stateChanged = new TrackerIsValidChangedEventArgs(this.gameObject.name, newValue);
        OnTrackerIsValidChanged(this, stateChanged);

    }

}
