﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using RealTracker;
using UnityEngine;
using Valve.VR;
using HiveTrackingServerSocket;

public class ServerSocketHandler : MonoBehaviour
{
    System.Threading.Thread SocketThread;
    volatile bool keepReading = false;

    public TrackingServerSocket serverSocket;

    void Awake()
    {
        Application.runInBackground = true;
        serverSocket = TrackingServerSocket.Instance;
        startServer();
    }
    
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void startServer()
    {
        SocketThread = new System.Threading.Thread(listen);
        SocketThread.IsBackground = true;
        SocketThread.Start();
    }

    void listen()
    {
        serverSocket.StartListener();
    }

    void stopServer()
         {
             keepReading = false;
     
             //stop thread
             if (SocketThread != null)
             {
                 SocketThread.Abort();
             }
     
             //TODO disconnect sockert properly
             /*if (handler != null && handler.Connected)
             {
                 handler.Disconnect(false);
                 Debug.Log("Disconnected!");
             }*/
         }

    void OnDisable()
    {
        stopServer();
    }

   /* public  void sendToDevice(string deviceID, string message)
    {
        serverSocket.devices.SendToDevice(deviceID, message);
    }*/
    
}
